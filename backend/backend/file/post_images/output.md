#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
#

# Sanchit Gupta's Resume

table {
width: 100%;
border-collapse: collapse;
}
th, td {
border: 1px solid black;
padding: 8px;
text-align: left;
}

# Sanchit Gupta's Resume

|Name|Sanchit Gupta|
|---|---|
|Location|Lucknow, India|

# Education

|Institution|Indian Institute Of Information Technology, Lucknow|
|---|---|
|Program|Bachelor of Technology in Computer Science and Business|
|CGPA|7.87|
|Duration|Dec. 2021 - April 2025|

# Projects

# Crop Yield Prediction

|Technology|Python, Machine Learning Algorithms, HTML, CSS|
|---|---|
|Description|A machine learning project predicting crop yield based on various factors.|

# Blogs Site

|Technology|Python, Django, Bootstrap, SQLite|
|---|---|
|Description|A web application for creating and managing blog posts.|

# The Yoga Instructor

|Technology|Numpy, Pandas, Matplotlib, OpenCV|
|---|---|
|Description|A deep learning model for yoga posture estimation and guidance.|

# Portfolio website

|Technology|Vitepress|
|---|---|
|Description|A personal portfolio site showcasing coding projects and skills.|

# Programming Skills

|Languages|C, C++, Python, Java, Dart, SQL|
|---|---|
|Technologies|Django, Django REST Framework, PostgreSQL, SQLite, HTML, CSS, JavaScript, Docker|
|Area of Interest|Problem-Solving, Competitive Programming, Back-end Development, Machine learning|

# Achievements

|Leetcode|Knight (Max rating: 1891)|
|---|---|
|CodeForce|Rated Specialist|
|Codechef|Rated 4 star's|
|DSA|Solved around 1000 problems from various coding platforms|
